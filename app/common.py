from sqlalchemy.orm import Session
from database import engine, SessionLocal
from models import Stock
import yfinance as yf

def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


def fetch_stock_data(id:int):
    db = SessionLocal()
    stock = db.query(Stock).filter(Stock.id == id).first()

    stock_data = yf.Ticker(stock.symbol).info

    stock.forward_pe = stock_data['forwardPE']
    stock.forward_eps = stock_data['forwardEps']
    stock.dividend_yield = stock_data['dividendYield']*100 if stock_data['dividendYield'] else stock_data['dividendYield']
    stock.ma50 = stock_data['fiftyDayAverage']
    stock.ma200 = stock_data['twoHundredDayAverage']
    stock.price = stock_data['previousClose']

    db.add(stock)
    db.commit()
