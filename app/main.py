from typing import Optional
from fastapi.templating import Jinja2Templates
from fastapi import FastAPI,Request,Depends,BackgroundTasks
from sqlalchemy.orm import Session
from database import engine
from requestModels import StockRequest
from common import get_db, fetch_stock_data
import models
from models import Stock


app = FastAPI()
models.Base.metadata.create_all(bind=engine)
templates = Jinja2Templates(directory='templates')


@app.get("/")
async def home(request : Request, dividend_yield=None, forward_pe=None, ma50=None, ma200=None, db:Session = Depends(get_db)):
    '''
    Display the stock screener dashboard / Home Page.
    '''

    stocks = db.query(Stock)

    if dividend_yield:
        stocks = stocks.filter(Stock.dividend_yield<=dividend_yield)
    if forward_pe:
        stocks = stocks.filter(Stock.forward_pe<=forward_pe)
    if ma50:
        stocks = stocks.filter(Stock.price>=Stock.ma50)
    if ma200:
        stocks = stocks.filter(Stock.price>=Stock.ma200)

    return templates.TemplateResponse("home.html", {"request": request,
                                                    "stocks":stocks})



@app.post("/stock")
async def create_stock(stock_request:StockRequest, background_task:BackgroundTasks, db:Session = Depends(get_db)):
    '''
    Create a stock and saves it in Database
    '''

    stock = db.query(Stock).filter_by(symbol = stock_request.symbol).first()

    if stock:
        response = {"code" : "Success","message" : "Stock Updated"}

    else:
        stock = Stock()
        stock.symbol = stock_request.symbol
        db.add(stock)
        db.commit()
        response = {"code" : "Success","message" : "Stock Created"}

    background_task.add_task(fetch_stock_data, stock.id)

    return response
