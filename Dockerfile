FROM python:3.7

COPY ./app /app

RUN pip install -r /app/requirments.txt

EXPOSE 80

WORKDIR /app

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]

